// Digispark as a Mouse jiggler
// Based on the Digispark-Mouse example sketch
// Originally created by Sean Murphy (duckythescientist)

#include <DigiMouse.h>

uint8_t state;

void setup() {
  pinMode(1, OUTPUT);
  DigiMouse.begin();
}

void loop() {
  // If not using plentiful DigiMouse.delay(), make sure to call
  // DigiMouse.update() at least every 50ms
  /*
   * this code draws a diamond shape below the cursor, over and over
   * 
   * bit       X   Y
   * 76543210
   * 00xxxxxx  1,  1
   * 01xxxxxx -1,  1
   * 10xxxxxx -1, -1
   * 11xxxxxx  1, -1
   */
  DigiMouse.moveX(bitRead(state, 7) ^ bitRead(state, 6) ? -1 : 1);
  DigiMouse.moveY(bitRead(state, 7) ? -1 : 1);

  // blink with 5Hz at 1/255 PWM
  analogWrite(1, bitRead(state, 0));
  
  DigiMouse.delay(100);
  
  // the state overflows intentionally
  state++;
}
